import XMonad
import XMonad.Actions.CycleWS
import XMonad.Actions.GridSelect
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers(doRectFloat)
import XMonad.Hooks.ScreenCorners
import XMonad.Hooks.SetWMName
import XMonad.Layout.Dwindle
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import qualified XMonad.StackSet as W


myBorderWidth = 2

myEventHook e = do
    -- handleEventHook def <+> XMonad.Hooks.EwmhDesktops.fullscreenEventHook
    screenCornerEventHook e

myFocusedBorderColor = "#009933"

myLayout = screenCornerLayoutHook $
    Spiral L XMonad.Layout.Dwindle.CW 1.62 (11/10)
    ||| tiled
    ||| Mirror tiled
    ||| noBorders( Full )
    where
        -- default tiling algorithm partitions the screen into two panes
        tiled   = spacing 2 $ Tall nmaster delta ratio

        -- The default number of windows in the master pane
        nmaster = 1

        -- Default proportion of screen occupied by master pane
        ratio   = 62/100

        -- Percent of screen to increment by when resizing panes
        delta   = 2/100

myManageHook = composeAll
    [
        stringProperty "_NET_WM_NAME" =? "Emulator" --> doRectFloat(W.RationalRect 0.1 0.1 0.3 0.8)
    ]
myModMask     = mod4Mask
myNormalBorderColor = "#663300"
myTerminal    = "termite"
myWorkspaces = ["1:tmp", "2:web1", "3:code", "4:web2", "5:opt1", "6:opt2", "7:opt3", "8:opt4", "9:opt5"]

myStartupHook = do
    setWMName "LG3D"
    addScreenCorner SCUpperRight (goToSelected defaultGSConfig { gs_cellwidth = 200})
    addScreenCorners [ (SCLowerRight, nextWS)
                     , (SCLowerLeft,  prevWS)
                     ]


main = do
    xmonad $ ewmh defaultConfig
        { borderWidth        = myBorderWidth
        , focusedBorderColor = myFocusedBorderColor
        -- , handleEventHook    = handleEventHook def <+> XMonad.Hooks.EwmhDesktops.fullscreenEventHook
        , handleEventHook    = myEventHook
        , layoutHook         = myLayout
        , manageHook = myManageHook
        , modMask            = myModMask
        , normalBorderColor  = myNormalBorderColor
        , startupHook        = myStartupHook
        , terminal           = myTerminal        
        , workspaces = myWorkspaces
        }


-- vim:sw=4 sts=4 ts=4 tw=0 et ai
