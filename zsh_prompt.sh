autoload -U promptinit
promptinit

# prompt minimal 'git clone https://github.com/subnixr/minimal.git ~/zsh/minimal'

MNML_USER_CHAR='$'
MNML_NOMRAL_CHAR='V'
MNML_INSERT_CHAR='>'

# Components on the left prompt
MNML_PROMPT=(mnml_ssh mnml_pyenv 'mnml_cwd 2 0' mnml_status mnml_keymap)

# Components on the right prompt
MNML_RPROMPT=(mnml_git)

# Components shown on info line
MNML_INFOLN=(mnml_err mnml_jobs mnml_uhp mnml_files)

MNML_MAGICENTER=(mnml_me_dirs mnml_me_ls mnml_me_git)

source ~/zsh/minimal/minimal.zsh
