# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Put your fun stuff here.
export PS1="\n"$PS1

export PYTHONPATH=$HOME/src

# Command history
export HISTFILESIZE=8192
export HISTSIZE=4096
shopt -s histappend   # agrega los comandos al archivo de historia en lugar de sobreescribrlo

[ -f /etc/profile.d/bash-completion.sh ] && source /etc/profile.d/bash-completion.sh  # es lento

# Aliases
# -------
alias hi='history'

# busca solo en archivos fuente de Python
alias grepy='grep --include="*.py" --line-number --binary-files=without-match --recursive'

# busca solo en archivos XML
alias grepx='grep --include="*.xml" --line-number --binary-files=without-match --recursive'

# busca solo en archivos Kid
alias grepk='grep --include="*.kid" --line-number --binary-files=without-match --recursive'

# graba la historia de comandos y la vuelve a leer
alias hire='history -a; history -n'

# abre con gvim todos los archivos Modificados o Agregados de acuerdo a svn st
alias gvimam='gvim `svn st| grep "^[AM].*\.py$"| cut -d " " -f 8`'

# lista los archivos modificados el dia de hoy
alias svnhoy='svn st |cut -b 9-|xargs ls -lh|grep `date +%Y-%m-%d`'

# Hora de entrada hoy
alias hin='last facundo |grep "`date +"%b %_d"`" |sort -k 6'

# Window Manager Control, busca y activa una ventana
alias wa="wmctrl -a"

# Buscar palabra en diccionario
function dict() {
    curl dict://dict.org/d:$1
}

# Fierro: poner clave de usuario 'admin' en 'a'
function fierroadminpass() {
    psql $1 -c "UPDATE users SET password = 'a' WHERE user_name = 'admin';"
}

# Fierro: buscar la definicion de una clase en colofon (correr desde src)
function ficlass() {
    grep --include="*.py" --line-number --binary-files=without-match --recursive \
         "class $1\>" colofon
}

# Fierro: buscar la definicion de una funcion/metodo en colofon (correr desde src)
function fidef() {
    grep --include="*.py" --line-number --binary-files=without-match --recursive \
         "def $1\>" colofon
}

# Fierro: reiniciar servidor web, servidor Fierro y abrir cliente
function fiRestartAll {
    sudo /etc/init.d/apache2 restart && ./restartServer && ./fierro_es
}

# Add ssh tab completion from ~/.ssh/config Hosts
#SSH_COMPLETE=( $(grep "^Host " ~/.ssh/config |cut -d " " -f 2))complete -o default -W "${SSH_COMPLETE[*]}" ssh

function tussh() {
    ssh `grep $1 /home/facundo/fierro/tuneles`
}

function distalsalog() {
    #Ver el log de fierroserver-distalsa en distal-142
    ssh gnarvaja@jujuy "ssh distal-142 \"tail -f /var/log/fierroserver-distalsa.log\""
}

function distalsacplog() {
    #Copiar las entradas del log de distalsa de hoy a /home/public/tmp
    ssh gnarvaja@jujuy "ssh distal-142 \"grep ^`date +%d/%m/%y` /var/log/fierroserver-distalsa.log \
                                            > /tmp/fierroserver-distalsa-`date +%Y%m%d`.log\" \
                        && scp distal-142:/tmp/fierroserver-distalsa-`date +%Y%m%d`.log /home/public/tmp"
}

fortune -c
