export GREP_COLOR='1;32'
export XDG_CONFIG_HOME="/home/facundo/.config"
export PATH="${PATH}:/home/facundo/bin:/home/facundo/.local/bin"
export ANDROID_SDK_ROOT="/home/facundo/Android/Sdk"

# Wayland support
export MOZ_ENABLE_WAYLAND=1

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
