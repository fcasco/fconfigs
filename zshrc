# The following lines were added by compinstall
fpath=(~/.zsh/completion $fpath)

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' completions 1
zstyle ':completion:*' glob 1
zstyle ':completion:*' matcher-list '' \
  'm:{a-z\-}={A-Z\_}' \
  'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
  'r:|?=** m:{a-z\-}={A-Z\_}'
zstyle ':completion:*' max-errors 4
zstyle ':completion:*' substitute 1
zstyle :compinstall filename '/home/facundo/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=16384
SAVEHIST=8192
setopt appendhistory
setopt HIST_IGNORE_DUPS
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

fpath=(~/.zsh/completion $fpath)

autoload -U promptinit
promptinit
source ~/fconfigs/zsh_prompt.sh

DIRSTACKFILE="$HOME/.cache/zsh/dirs"
if [[ -f $DIRSTACKFILE ]] && [[ $#dirstack -eq 0 ]];
then
    distack=( ${(f)"$(< $DIRSTACKFILE)"} )
    [[ -d $dirstack[1] ]] && cd $dirstack[1]
fi
chpwd() {
    print -l $PWD ${(u)dirstack} >$DIRSTACKFILE
}

DIRSTACKSIZE=24

setopt autopushd pushdsilent pushdtohome
setopt pushdignoredups
setopt pushdminus

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

alias ls='ls --color=auto'

bindkey "^R" history-incremental-search-backward

source ~/.profile

# SSH Agent
if ! pgrep -u "$USER" ssh-agent > /dev/null
then
    ssh-agent > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
source "$XDG_RUNTIME_DIR/ssh-agent.env"

source /home/facundo/.config/broot/launcher/bash/br

# Google Cloud
source /home/facundo/gcloudrc.sh

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
