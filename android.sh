# Android development

export ANDROID_SDK_ROOT='/mnt/ssd/opt/android-sdk'
export ANDROID_HOME=$ANDROID_SDK_ROOT
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

# $ ls -l $ANDROID_HOME                                                                                                                ◢
# total 40
# drwxr-xr-x  5 facundo facundo 4096 Jul 12 15:42 build-tools
# drwxr-xr-x  7 facundo facundo 4096 Jun 19 14:48 emulator
# drwxr-xr-x  2 facundo facundo 4096 Jun 19 14:57 licenses
# drwxr-xr-x  3 facundo facundo 4096 Jun 19 14:42 patcher
# drwxr-xr-x  4 facundo facundo 4096 Jul 12 11:38 platforms
# drwxr-xr-x  5 facundo facundo 4096 Aug 24 11:45 platform-tools
# drwxr-xr-x 27 facundo facundo 4096 Jul 18 21:55 skins
# drwxr-xr-x  3 facundo facundo 4096 Jun 19 14:57 sources
# drwxr-xr-x  4 facundo facundo 4096 Jul 12 11:44 system-images
# drwxr-xr-x  6 facundo facundo 4096 Jun 19 14:42 tools
