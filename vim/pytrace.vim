"Le da un formato mas legible a un traceback de Python"

:set number
:set filetype=python
:%s/^/# /e
:%s/^.*"\(.\{-}\)",\ line\ \(\d\+\)\(.*\)$/\r\1 +\2 #\3/e
:%s-\\-/-ge
:%s/\.pyo/\.py/ge
:colorscheme ChocolateLiquor
