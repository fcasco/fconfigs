"Dart
setlocal autoindent
setlocal colorcolumn=96
setlocal expandtab
setlocal shiftwidth=2
setlocal smarttab
setlocal softtabstop=2
setlocal tabstop=2
setlocal textwidth=99

setlocal makeprg=dart\ %

syntax on

nmap <leader><cr> :!dart %<cr>
nmap <leader>t :!dart test --no-color --reporter expanded %<cr>


let syntastic_dart_checkers=[]

setlocal omnifunc=ale#completion#OmniFunc
