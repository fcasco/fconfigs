let g:syntastic_filetype_map = {"javascriptreact": "javascript"}
let g:syntastic_javascript_checkers = ['eslint']
set suffixesadd=.js,.jsx
