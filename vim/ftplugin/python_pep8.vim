" Python filetype plugin for running pep8
" Based on python_pyflakes from Vincent Driessen <vincent@datafox.nl>

if exists("b:loaded_pep8_ftplugin")
    finish
endif
let b:loaded_pep8_ftplugin=1

let s:pep8_cmd="pep8.py"

if !exists("*Pep8()")
    function Pep8()
        if !executable(s:pep8_cmd)
            echoerr "File " . s:pep8_cmd . " not found. Please install it first."
            return
        endif

        set lazyredraw   " delay redrawing
        cclose           " close any existing cwindows

        " store old grep settings (to restore later)
        let l:old_gfm=&grepformat
        let l:old_gp=&grepprg

        " write any changes before continuing
        if &readonly == 0
            update
        endif

        " perform the grep itself
        let &grepformat="%f:%l:%m"
        let &grepprg=s:pep8_cmd
        silent! grep! %

        " restore grep settings
        let &grepformat=l:old_gfm
        let &grepprg=l:old_gp

        " open cwindow
        let has_results=getqflist() != []
        if has_results
            execute 'belowright copen'
        endif

        set nolazyredraw
        redraw!

        if has_results == 0
            " Show OK status
            hi Green ctermfg=green
            echohl Green
            echon "Pep8: Coding style analysis OK"
            echohl
        endif
    endfunction
endif

nmap <F5> :call Pep8()<CR>
" Add mappings, unless the user didn't want this.
" The default mapping is registered under to <F7> by default, unless the user
" remapped it already (or a mapping exists already for <F7>)
"if !exists("no_plugin_maps") && !exists("no_pyflakes_maps")
"    if !hasmapto('Pyflakes()')
"        noremap <buffer> <F7> :call Pyflakes()<CR>
"        noremap! <buffer> <F7> :call Pyflakes()<CR>
"    endif
"endif
