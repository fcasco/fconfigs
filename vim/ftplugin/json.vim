setlocal autoindent
setlocal colorcolumn=96
setlocal expandtab
setlocal shiftwidth=4
setlocal smarttab
setlocal softtabstop=4
setlocal tabstop=4
setlocal textwidth=99

setlocal filetype=json

syntax on

" ALE
let b:ale_linters = ['jq']

let g:syntastic_javascript_checkers = ['jq']
