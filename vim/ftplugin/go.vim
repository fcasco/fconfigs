set rtp+=$GOPATH/src/github.com/golang/lint/misc/vim
setlocal makeprg=go\ run\ %
let g:syntastic_go_checkers = ['golint', 'govet', 'errcheck']

