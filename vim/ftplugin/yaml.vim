setlocal colorcolumn=96
setlocal expandtab
setlocal indentkeys-=0#
setlocal indentkeys-=<:>
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal tabstop=2

let g:syntastic_yaml_checkers = ['yamllint']


" comment/uncomment selected lines
vmap ,, :s/^/# /<cr>:noh<cr>
vmap ,<space> :s/^# //<cr>:noh<cr>
