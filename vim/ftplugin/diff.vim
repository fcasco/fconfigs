setlocal foldexpr=(getline(v:lnum)=~'^Index')?0:(getline(v:lnum)=~'^=\\{3,}')?1:((getline(v:lnum)=~'^@@')?'>2':'=')
setlocal foldmethod=expr

