let g:syntastic_sh_shellcheck_args="-x"
setlocal colorcolumn=96
setlocal expandtab
setlocal shiftwidth=4
setlocal smarttab
setlocal softtabstop=4
setlocal tabstop=4
setlocal textwidth=96

syntax on

",, comment out some code
vmap ,, :s/^/# <cr>:noh<cr>

",<space> remove comment from line start
vmap ,<space> :s/^# //<cr>:noh<cr>


setlocal suffixesadd=.py
setlocal comments=b:#,fb:-
setlocal commentstring=#\ %s

setlocal foldmethod=indent
