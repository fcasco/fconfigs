setlocal autoindent
setlocal colorcolumn=96
setlocal expandtab
setlocal shiftwidth=4
setlocal smarttab
setlocal softtabstop=4
setlocal tabstop=4
setlocal textwidth=100

syntax on
syntax match pythonPuctuation "[\[\](){}:\.,;]"

setlocal makeprg=python\ %
setlocal keywordprg=pydoc

",, comment out some code
vmap ,, :s/^/# <cr>:noh<cr>

",<space> remove comment from line start
vmap ,<space> :s/^# //<cr>:noh<cr>

"autocopmlete in insert mode with [ctrl]+[space]
imap <c-space> <C-x><C-k>

nmap <leader>d :!python -m doctest %<CR>


" ALE
let b:ale_linters = ['flake8']


" let g:syntastic_python_python_exec = '/usr/bin/python2'
let g:syntastic_python_checkers = ['flake8']


" Insert Pytyhon debuggers
nmap <Leader>up Ofrom pdb import set_trace; set_trace()<esc>
nmap <Leader>ui Ofrom ipdb import set_trace; set_trace()<esc>
nmap <Leader>uu Ofrom pudb import set_trace; set_trace()<esc>

setlocal path=.,./..,,
setlocal include=^\\s*\\(from\\\|import\\)

" For imports with leading .., append / and replace additional .s with ../
let b:grandparent_match = '^\(.\.\)\(\.*\)'
let b:grandparent_sub = '\=submatch(1)."/".repeat("../",strlen(submatch(2)))'

" For imports with a single leading ., replace it with ./
let b:parent_match = '^\.\(\.\)\@!'
let b:parent_sub = './'

" Replace any . sandwiched between word characters with /
let b:child_match = '\(\w\)\.\(\w\)'
let b:child_sub = '\1/\2'

setlocal includeexpr=substitute(substitute(substitute(
      \v:fname,
      \b:grandparent_match,b:grandparent_sub,''),
      \b:parent_match,b:parent_sub,''),
      \b:child_match,b:child_sub,'g')

setlocal suffixesadd=.py
setlocal comments=b:#,fb:-
setlocal commentstring=#\ %s

setlocal wildignore+=*.pyc

setlocal foldmethod=indent
