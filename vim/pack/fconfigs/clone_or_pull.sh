#!/bin/sh

for repo_url in $(cat repos.txt)
do
    repo_filename="${repo_url##*/}"
    repo_path="${repo_filename%.git}"
    echo "Checking $repo_url at $repo_path ..."
    if [ -d "$repo_path" ]
    then
        cd "$repo_path"
        git pull
        cd -
    else
        git clone --depth 1 "$repo_url"
    fi
    echo ""
done
