local wezterm = require 'wezterm';

return {
    color_scheme = "Gruvbox Dark",
    dpi = 192.0,
    font = wezterm.font("Ubuntu Mono"),
    font_size = 11,
    window_background_opacity = 0.90,
    debug_key_events=true,
}
